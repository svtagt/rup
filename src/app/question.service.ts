import { Injectable }       from '@angular/core';

import { DropdownQuestion } from './question-dropdown';
import { QuestionBase } from './question-base';
import { TextboxQuestion } from './question-textbox';

@Injectable()
export class QuestionService {

  // TODO: get from a remote source of question metadata
  // TODO: make asynchronous
  getQuestions() {

    let questions: QuestionBase<any>[] = [

      new TextboxQuestion({
        key: 'firstName',
        label: 'На каком расстоянии от мест хранения известкового ила, удаляемого из ацетиленового генератора, разрешается курить или пользоваться открытым огнем? (в метрах)',
        order: 1
      }),

      new TextboxQuestion({
        key: 'textQuestion6',
        label: 'На каком расстоянии от металлических печей можно располагать оборудование? (в метрах)',
        order: 2
      }),

      new DropdownQuestion({
        key: 'dropdownQuestion1',
        label: 'Какой федеральный закон определяет основы обеспечения пожарной безопасности?',
        options: [
          {key: 'answer1',  value: '69-ФЗ "О пожарной безопасности"'},
          {key: 'answer2',  value: '116-ФЗ "О промышленной безопасности опасных производственных объектов"'},
          {key: 'answer3',   value: '390-ФЗ "О безопасности"'},
          {key: 'answer4', value: '123-ФЗ "Технический регламент о требованиях пожарной безопасности"'}
        ],
        order: 3
      }),

      new DropdownQuestion({
        key: 'dropdownQuestion2',
        label: 'Какое взрывозащищенное электрооборудование относится к 1 уровню взрывозащиты?',
        options: [
          {key: 'answer1',  value: 'Особовзрывобезопасное электрооборудование'},
          {key: 'answer2',  value: 'Взрывобезопасное электрооборудование'},
          {key: 'answer3',   value: 'Электрооборудование повышенной надежности против взрыва'},
          {key: 'answer4', value: 'Взрывонепроницаемое электрооборудование'}
        ],
        order: 4
      }),

      new TextboxQuestion({
        key: 'textQuestion3',
        label: 'Какое количество карбида кальция разрешается хранить при отсутствии промежуточного склада хранения?',
        order: 5
      }),

      new DropdownQuestion({
        key: 'dropdownQuestion3',
        label: 'Что запрещается на погрузочной площадке во время слива и налива СУГ?',
        options: [
          {key: 'answer1',  value: 'Проводить пожароопасные работы и курить на расстоянии менее 100 м от цистерны'},
          {key: 'answer2',  value: 'Проведение ремонтных работ на цистернах и вблизи них'},
          {key: 'answer3',   value: 'Подъезд автомобильного и маневрового железнодорожного транспорта'},
          {key: 'answer4', value: 'Нахождение на сливоналивной эстакаде посторонних лиц, не имеющих отношения к сливоналивным операциям'}
        ],
        order: 6
      }),

      new TextboxQuestion({
        key: 'textQuestion4',
        label: 'Пожар какого класса можно потушить водным огнетушителем?',
        order: 7
      }),

      new DropdownQuestion({
        key: 'dropdownQuestion4',
        label: 'Каким образом происходит автоматическое включение дренчерных установок?',
        options: [
          {key: 'answer1',  value: 'По сигналу побудительной системы'},
          {key: 'answer2',  value: 'По сигналу установок пожарной сигнализации'},
          {key: 'answer3',   value: 'По сигналу датчиков технологического оборудования'},
          {key: 'answer4', value: 'По сигналу одного из указанного технического средства'}
        ],
        order: 8
      }),

      new DropdownQuestion({
        key: 'dropdownQuestion5',
        label: 'Для каких целей запрещается использовать чердаки и вентиляционные камеры?',
        options: [
          {key: 'answer1',  value: 'Для организации производственных участков'},
          {key: 'answer2',  value: 'Для организации хранения продукции'},
          {key: 'answer3',   value: 'Для организации хранения оборудования'},
          {key: 'answer4', value: 'Запрещается для всех этих целей использовать чердаки и вентиляционные камеры'}
        ],
        order: 9
      }),

      new TextboxQuestion({
        key: 'textQuestion5',
        label: 'Электрооборудование с каким максимальным напряжением можно тушить углекислотным огнетушителем? (кВ)',
        order: 10
      }),

      new DropdownQuestion({
        key: 'dropdownQuestion6',
        label: 'Как часто должна осуществляться проверка работоспособности сетей противопожарного водопровода?',
        options: [
          {key: 'answer1',  value: 'Один раз в год'},
          {key: 'answer2',  value: 'Не реже двух раз в год (весной и осенью)'},
          {key: 'answer3',   value: 'Не реже одного раза в два года'},
          {key: 'answer4', value: 'Не реже трех раз в год (летом)'}
        ],
        order: 11
      }),


    ];

    return questions.sort((a, b) => a.order - b.order);
  }
}
