import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent {
  sidebarItems = [
    {
      title: 'Главная',
      type: 'link',
      url: '/main',
      icon: 'fa fa-sitemap',
      active: false,
      toggle: false
    },

    {
      title: 'Механизмы возникновения и развития пожаров',
      type: 'link',
      url: '/chapter1',
      icon: 'fa fa-sitemap',
      active: false,
      toggle: false
    },

    {
      title: 'Пожарная безопасность',
      type: 'link',
      url: '/chapter2',
      icon: 'fa fa-sitemap',
      active: false,
      toggle: false
    },

    {
      title: 'Горение как химическая реакция',
      type: 'link',
      url: '/chapter3',
      icon: 'fa fa-sitemap',
      active: false,
      toggle: false
    },

    {
      title: 'Классификация и причины пожаров',
      type: 'link',
      url: '/chapter4',
      icon: 'fa fa-sitemap',
      active: false,
      toggle: false
    },

    {
      title: 'Действия и правила поведения при пожаре',
      type: 'link',
      url: '/chapter5',
      icon: 'fa fa-sitemap',
      active: false,
      toggle: false
    },

    {
      title: 'Защита предприятий и населения от факторов пожаров',
      type: 'link',
      url: '/chapter6',
      icon: 'fa fa-sitemap',
      active: false,
      toggle: false
    },

    {
      title: 'Итоговое Тестирование',
      type: 'link',
      url: '/testing',
      icon: 'fa fa-sitemap',
      active: false,
      toggle: false
    },
  ];
}
