import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {MainComponent} from './main/main.component';
import {Chapter1Component} from './chapter1/chapter1.component';
import {Chapter5Component} from './chapter5/chapter5.component';
import {Chapter4Component} from './chapter4/chapter4.component';
import {Chapter3Component} from './chapter3/chapter3.component';
import {Chapter2Component} from './chapter2/chapter2.component';
import {Chapter6Component} from './chapter6/chapter6.component';
import {TestingComponent} from './testing/testing.component';

const routes: Routes = [
  {
    path: '', redirectTo: '/main', pathMatch: 'full'
  },
  {
    path: 'main', component: MainComponent
  },
  {
    path: 'chapter1', component: Chapter1Component
  },
  {
    path: 'chapter2', component: Chapter2Component
  },
  {
    path: 'chapter3', component: Chapter3Component
  },
  {
    path: 'chapter4', component: Chapter4Component
  },
  {
    path: 'chapter5', component: Chapter5Component
  },
  {
    path: 'chapter6', component: Chapter6Component
  },
  {
    path: 'testing', component: TestingComponent
  },
];

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule
  ]
})

export class AppRoutingModule { }
